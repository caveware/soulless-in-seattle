extends Node

onready var action = $action
onready var camera = $camera
onready var background = $background
onready var ghost = $ghost

onready var axe = $props/axe
onready var cat = $props/cat
onready var cheese = $props/cheese
onready var dog = $props/dog
onready var haunt_marker = $props/haunt_marker
onready var lnm = $props/lnm
onready var poison = $props/poison
onready var rat = $props/rat
onready var rat_hole = $props/rat_hole
onready var tree = $props/tree
onready var water = $props/water

onready var blinds = $canvas/blinds
onready var convo = $canvas/convo

var active_haunt = null
var hauntable = preload("res://skelentities/hauntables/_base_hauntable.gd")
var carpet_metal = load("res://skelentities/props/metal/carpet_metal.tscn")
var new_rat_class = load("res://skelentities/hauntables/rat/rat.tscn")
var stairs_down = preload("res://skelentities/stairs/stairs_down.gd")
var stairs_up = preload("res://skelentities/stairs/stairs_up.gd")

func _ready():
	axe.connect("GOT", self, "_on_axe_get")
	cat.connect("COMPLETE", self, "_on_cat_complete")
	cat.connect("START_CONVO", self, "_on_cat_convo")
	cheese.connect("POISONING", self, "_on_poison_cheese_start")
	cheese.connect("RAT", self, "_on_poison_rat_start")
	dog.connect("COMPLETE", self, "_on_dog_complete")
	dog.connect("START_CONVO", self, "_on_dog_convo")
	lnm.connect("ADD_METAL", self, "_on_metal_add")
	poison.connect("POISONED_CHEESE", self, "_on_poison_cheese_end")
	tree.connect("ON_CHOP", self, "_post_chop")
	tree.connect("DONE_CHOPPING", self, "_done_chop")

	blinds.connect("OPENED", self, "_on_start")

func _physics_process(delta):
	var current_area
	var current_area_parent
	var areas = ghost.detector.get_overlapping_areas()
	if areas.size() == 0 or not ghost.is_active or not ghost.allow:
		# Cancel text
		action.visible = false
	else:
		# Show text
		current_area = areas[0]
		current_area_parent = current_area.get_parent()
		if "position" in current_area_parent:
			var offset = current_area_parent.text_offset
			action.rect_position = current_area_parent.position + offset
			action.text = current_area.get_parent().action
			action.visible = true
		elif current_area_parent.get_active():
			var offset = current_area_parent.text_offset
			action.rect_position = current_area_parent.rect_position + offset
			action.text = current_area_parent.action
			action.visible = true

			if not current_area_parent.get_active():
				action.visible = false

	if Input.is_action_just_pressed("ui_accept"):
		if ghost.is_active:
			if current_area:
				var pool = background.get_children()

				if current_area_parent is stairs_up or current_area_parent is stairs_down:
					for item in pool:
						if "id" in item:
							var id = item.id
							if item != current_area_parent and id == current_area_parent.id:
								ghost.position.x = item.position.x + 16
								ghost.position.y = item.position.y + 40
								break

				elif current_area_parent is hauntable:
					active_haunt = current_area_parent
					active_haunt.is_active = true
					active_haunt.velocity.y = -64
					ghost.is_active = false
					ghost.visible = false
					ghost.on_haunt()

					haunt_marker.start = ghost.position
					haunt_marker.end = active_haunt.position
					haunt_marker.timer = 0
					haunt_marker.direction = 1

				else:
					current_area_parent.use()

	if Input.is_action_just_pressed("ui_cancel"):
		if active_haunt and active_haunt.is_active:
			dehaunt()

	if ghost.is_active:
		camera.position = ghost.position + Vector2(0, -24)
		if abs(ghost.position.x - rat_hole.position.x) < 40:
			rat_hole.hide_rat()
		else:
			rat_hole.show_rat()

		if abs(ghost.position.x - lnm.rect_position.x + 16) < 16:
			lnm.show_lnm()
		else:
			lnm.hide_lnm()
	else:
		camera.position = active_haunt.position + Vector2(0, -8)
		rat_hole.show_rat()

		if active_haunt.is_active:
			lnm.hide_lnm()

	# Camera placement
	var pos = active_haunt.position if active_haunt else ghost.position

	if pos.x >= 344:
		# If outside
		camera.position.x = 416
		camera.position.y -= 32

		if camera.position.y > 112 and convo.visible:
			camera.position.y += 32
	else:
		camera.position.x = min(pos.x, 296)
		camera.position.y = 184 if pos.y > 112 else 72


func dehaunt():
	ghost.position.x = active_haunt.position.x
	ghost.on_unhaunt()

	haunt_marker.start = ghost.position
	haunt_marker.end = active_haunt.position
	haunt_marker.timer = 1
	haunt_marker.direction = -1

	ghost.is_active = true
	ghost.visible = true
	active_haunt.is_active = false
	active_haunt = null

func _on_poison_cheese_start():
	active_haunt.is_active = false
	active_haunt = cheese
	poison._tween_up(cheese.position)

func _on_poison_cheese_end():
	poison.queue_free()
	cheese.do_poison()
	active_haunt = cheese
	cheese.is_active = true

func _on_poison_rat_start():
	var tween = $tweens/give_cheese_to_rat

	active_haunt.is_active = false
	rat.visible = true
	rat_hole.rat_gone = true
	rat_hole.hide_rat()

	tween.interpolate_callback(cheese, 1, "take_cheese")
	tween.interpolate_callback(rat, 2, "make_right")
	tween.interpolate_callback(rat, 2.1, "make_left")
	tween.interpolate_callback(rat, 2.2, "make_right")
	tween.interpolate_callback(rat, 2.3, "make_left")
	tween.interpolate_callback(rat, 2.4, "make_right")
	tween.interpolate_callback(rat, 2.5, "make_left")
	tween.interpolate_callback(cheese, 3, "add_barf")
	tween.interpolate_callback(self, 4, "_make_barf_rat")
	tween.start()

func _make_barf_rat():
	var new_rat = new_rat_class.instance()
	new_rat.position = rat.position
	new_rat.connect("AT_LNM", self, "_show_monster_for_rat")
	$props.add_child(new_rat)
	rat.queue_free()
	active_haunt = new_rat
	active_haunt.is_active = true

func _show_monster_for_rat():
	active_haunt.is_active = false
	lnm.show_lnm()

	var tween = $tweens/rat_meets_lnm
	tween.interpolate_callback(self, 1, "_monster_lnm_react")
	tween.start()

func _monster_lnm_react():
	lnm.shrink_away()

func _on_metal_add():
	active_haunt.limits.x = rat_hole.position.x
	active_haunt.begin_end()
	dehaunt()

	var tween = $tweens/rat_meets_lnm
	_metalify_water($props/water/water_1)
	tween.interpolate_callback(self, .3, "_metalify_water", $props/water/water_2)
	tween.interpolate_callback(self, .6, "_metalify_water", $props/water/water_3)
	tween.start()

func _on_start():
	blinds.disconnect("OPENED", self, "_on_start")
	convo.start_convo([
		{ "text": "YOU STAND BEFORE THE COUNCIL OF CULINARY GHOSTS." },
		{ "text": "YOU MAY NOT BE AWARE, BUT YOU ARE IN PURGATORY." },
		{ "text": "TO ENTER FRESH FOOD VALHALLA, YOU MUST PASS OUR TEST." },
		{ "text": "..." },
		{ "text": "WE NEED A MASCOT." },
		{ "text": "HONESTLY, WE'RE QUITE OFF-BRAND FOR HALLOWEEN." },
		{ "text": "IF OUR MANAGER SEES US, WE'RE TOAST." },
		{ "text": "RETURN TO EARTH AND FIND THE LEAST SPOOKABLE ANIMAL." },
		{ "text": "WE HAVE SELECTED A VACANT HOUSE IN SEATTLE FOR YOU." },
		{ "text": "GO, AND SAVE OUR METAPHORICAL ASSES!" }
	])
	convo.connect("CONVO_COMPLETE", self, "_drop_ghost")

func _metalify_water(water_node):
	var metal = carpet_metal.instance()
	metal.position = water_node.position
	water_node.queue_free()
	water.add_child(metal)

func _drop_ghost():
	convo.disconnect("CONVO_COMPLETE", self, "_drop_ghost")
	var fall_tween = $tweens/fall_tween
	var target = Vector2(ghost.position.x, 216)
	fall_tween.interpolate_property(ghost, "position", ghost.position, target, 1,
                Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 1)
	fall_tween.interpolate_callback(self, 2, "_post_drop")
	fall_tween.start()

func _post_drop():
	ghost.activate()
	$bgm/ghosts.stop()
	$bgm/gameplay.play()

func _post_chop():
	ghost.position.x -= 16

	if tree.chop_level == tree.MAX_CHOPS:
		ghost.allow = false
		ghost.position.x -= 16
		ghost.velocity.x = 0.01

func _done_chop():
	convo.start_convo([
		{ "text": "YOU MAY HAVE DESTROYED MY NEST." },
		{ "text": "BUT SIR, YOU WILL NEVER SHAKE MY CONFIDENCE." },
		{ "text": "TODAY YOU HAVE SHOWN ME A COMPLETE LACK OF RESPECT." },
		{ "text": "BUT I WILL NOT FALTER." },
		{ "text": "FOR TOMORROW WILL COME AND I WILL MOVE ON." },
		{ "text": "BUT YOU WILL LIVE WITH THIS SHAME FOR LIFE." },
		{ "text": "... OR PERHAPS YOU WON'T." },
		{ "text": "YOU KNOW, EGOISM IS PRETTY POPULAR THESE DAYS." },
		{ "text": "MAYBE YOU DID THIS PURELY FOR YOUR OWN BENEFIT." },
		{ "text": "HAVE YOU HEARD OF MAX STIRNER?" },
		{ "text": "HE CALLS PROPERTY A 'SPOOK', A CONSTRUCT OF SOCIETY." },
		{ "text": "HE MAKES SOME QUITE COMPELLING POINTS ABOUT THIS." },
		{ "text": "THEY'RE QUITE ELABORATE SO I'LL SPARE YOU THE DEETS." },
		{ "text": "BUT I DISAGREE WITH EGOISM. PRAGMATISM IS MORE MY SPEED." },
		{ "text": "SO TO YOU I SAY FUCK OFF AND GOOD DAY." }
	])
	convo.connect("CONVO_COMPLETE", self, "_fade_to_end")

func _fade_to_end():
	blinds.opening = false
	blinds.connect("CLOSED", self, "_closed")

func _closed():
	get_tree().change_scene("res://skelevels/win.tscn")

func _on_dog_convo():
	ghost.allow = false
	convo.start_convo([
		{ "text": "YOU WANT TO ENTER MY MASTER'S DOMAIN?" },
		{ "text": "PERHAPS YOU SHOULD ANSWER MY THREE QUESTIONS, VAGRANT." },
		{ "text": "QUESTION 1: WHO'S A GOOD BOY?", "answers": [
			"YOU",
			"ME",
			"LITERALLY EVERYONE"
		] },
		{ "text": "A WILD CHOICE, BUT I GUESS I'LL ALLOW IT." },
		{ "text": "QUESTION 2: WHAT IS THE SPOOKIEST HOLIDAY?", "answers": [
			"CHRISTMAS",
			"HALLOWEEN",
			"EASTER"
		] },
		{ "text": "HUH. I WOULD SAY NEW YEARS EVE... I HATE FIREWORKS." },
		{ "text": "FINAL QUESTION: WHAT DO YOU THINK OF CATS?", "answers": [
			"I HATE THEM",
			"I PREFER DOGS",
			"THEY SHOULDN'T EXIST"
		] },
		{ "text": "OH MY GOD." },
		{ "text": "I CAN'T BELIEVE YOU JUST SAID THAT." },
		{ "text": "G-GOOD LUCK YOU V-V-VAGRANT." },
	])
	convo.connect("CONVO_COMPLETE", self, "_after_dog_convo")

func _after_dog_convo():
	convo.disconnect("CONVO_COMPLETE", self, "_after_dog_convo")
	dog.launch()

func _on_dog_complete():
	ghost.allow = true

func _on_cat_convo():
	ghost.allow = false
	if cat.rect_position.x > 32:
		convo.start_convo([
			{ "text": "THAT HURT ME. THAT HURT ME DEEP." },
			{ "text": "I'VE ALWAYS CONSIDERED YOUR KIND BENEVOLENT..." },
			{ "text": "...BUT NOW I CAN SEE YOU SLEEP ON A BED OF LIES." },
			{ "text": "ENJOY YOUR OBSTACLE COURSE, LOSER." }
		])
		convo.connect("CONVO_COMPLETE", self, "_cat_shut")
	else:
		convo.start_convo([
			{ "text": "I'M STILL MAD... BUT YOU DID GOOD." },
		])
		convo.connect("CONVO_COMPLETE", self, "_after_cat_convo")

func _cat_shut():
	convo.disconnect("CONVO_COMPLETE", self, "_cat_shut")
	blinds.opening = false
	blinds.connect("CLOSED", self, "_cat_open")

func _cat_open():
	blinds.disconnect("CLOSED", self, "_cat_open")
	cat.move()
	blinds.opening = true
	blinds.connect("OPENED", self, "_after_cat_convo")

func _after_cat_convo():
	blinds.disconnect("OPENED", self, "_after_cat_convo")
	convo.disconnect("CONVO_COMPLETE", self, "_after_cat_convo")
	ghost.allow = true

func _on_axe_get():
	tree.has_axe = true
	axe.queue_free()