extends Node

onready var blinds = $blinds

var can_close = false

func _ready():
	blinds.connect("CLOSED", self, "_begin_game")
	blinds.connect("OPENED", self, "_allow_input")

func _allow_input():
	can_close = true

func _begin_game():
	get_tree().change_scene("res://skelevels/world.tscn")

func _physics_process(delta):
	if can_close and Input.is_action_just_pressed("ui_accept"):
		can_close = false
		blinds.opening = false