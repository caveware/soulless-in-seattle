extends Container

const BOUNCE_SPEED_LEFT = -256
const BOUNCE_SPEED_RIGHT = 2048

var interval = .35 + randf() * 1.5

var action = "die"
var text_offset = Vector2(0, 0)

var ghost = load("res://skelentities/ghost/ghost.gd")

func _ready():
	play()

func play():
	$tween.interpolate_property($sprite, "position", Vector2(0, 0), Vector2(0, -50), .1, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$tween.interpolate_property($sprite, "position", Vector2(0, -50), Vector2(0, 0), .1, Tween.TRANS_LINEAR, Tween.EASE_OUT, interval)
	$tween.interpolate_callback(self, interval + .2, "play")
	$tween.start()

func get_active():
	return false

func _physics_process(delta):

	var bodies = $detector.get_overlapping_bodies()
	for body in bodies:
		if body is ghost:
			if body.position.x >= rect_position.x:
				body.velocity.x = BOUNCE_SPEED_RIGHT
			else:
				body.velocity.x = BOUNCE_SPEED_LEFT
	$detector.position.y = $sprite.position.y