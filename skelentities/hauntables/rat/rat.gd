extends "res://skelentities/hauntables/_base_hauntable.gd"

signal AT_LNM

var ending = false
var triggered_lnm = false

onready var sprite = $sprite

func _ready():
	bounces = false
	direction = "left"
	limits.y = 260

func _physics_process(delta):
	sprite.animation = "idle_%s" % direction
	if not triggered_lnm and position.x == limits.y:
		triggered_lnm = true
		emit_signal("AT_LNM")

	if ending and position.x == limits.x:
		queue_free()

func begin_end():
	direction = "left"
	ending = true
	$detector.monitorable = false
	$detector.monitoring = false

func _get_x_speed():
	if ending:
		return -4
	else:
		return 0