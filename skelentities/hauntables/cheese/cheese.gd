extends "res://skelentities/hauntables/_base_hauntable.gd"

signal POISONING
signal RAT

var has_collided = false
var has_rat = false

var poison = load("res://skelentities/hauntables/poison/poison.gd")

func _physics_process(delta):
	var collider = _get_collider()
	if not has_collided and collider and collider is poison:
		on_poison(collider)

	if not has_rat and $poison.visible and position.x == limits.y:
		on_rat()

func on_poison(poison):
	has_collided = true
	emit_signal("POISONING")

func on_rat():
	has_rat = true
	emit_signal("RAT")

func do_poison():
	$cheese.visible = false
	$poison.visible = true
	limits.y += 28

func take_cheese():
	collision_layer = 4
	collision_mask = 4
	$poison.visible = false
	$detector.monitoring = false
	$detector.monitorable = false

func add_barf():
	$barf.visible = true