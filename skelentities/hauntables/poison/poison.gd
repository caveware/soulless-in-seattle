extends "res://skelentities/hauntables/_base_hauntable.gd"

signal POISONED_CHEESE

onready var tween = $tween

var cheese = load("res://skelentities/hauntables/cheese/cheese.gd")
var target

func _physics_process(delta):
	var collider = _get_collider()
	if collider and collider is cheese and not collider.has_collided:
		collider.on_poison(self)

func _tween_up(goal):
	in_statis = true
	target = goal

	collision_layer = 4
	collision_mask = 4

	var pos = Vector2(target.x, target.y - 24)
	tween.interpolate_property(self, "position", position, pos, .25,
                Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.interpolate_property(self, "position", pos, target, .1,
                Tween.TRANS_LINEAR, Tween.EASE_IN, .35)
	tween.interpolate_callback(self, .45, "_tween_done")
	tween.start()

func _tween_done():
	emit_signal("POISONED_CHEESE")