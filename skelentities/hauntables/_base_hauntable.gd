extends KinematicBody2D

var GRAVITY = Vector2(0, 420)

var acceleration = 48
var action = "Haunt"
var bounces = true
var direction = "right"
var in_statis = false
var is_active = false
var mode = "idle"
var movement_speed = 32
var text_offset = Vector2(-80, -24)
var velocity = Vector2(0, 0)

export var limits = Vector2(-10000, 10000)

onready var detector = $detector

var collider setget ,_get_collider

func _physics_process(delta):

	if in_statis:
		return

	var x_speed = 0
	if is_active:
		if Input.is_action_pressed("ui_left"):
			x_speed = -1
			direction = "left"
		if Input.is_action_pressed("ui_right"):
			x_speed = 1
			direction = "right"

		if x_speed != 0 and is_on_floor() and bounces:
			velocity.y = -32
			$sounds/bounce.play()
	else:
		x_speed = _get_x_speed()

	velocity.x = lerp(velocity.x, x_speed * movement_speed, acceleration * delta)

	velocity += GRAVITY * delta

	velocity = move_and_slide(velocity, Vector2(0, -1))

	mode = "idle" if velocity.x == 0 else "walk"

	if position.x < limits[0]:
		position.x = limits[0]
	if position.x > limits[1]:
		position.x = limits[1]

func _get_collider():
	var count = get_slide_count()

	if count == 0:
		return

	return get_slide_collision(0).collider

func _get_x_speed():
	return 0