extends Node2D

signal CONVO_COMPLETE

const SCROLL_SPEED = 1

onready var text = $text

var answers = []
var data = []
var selected = 0
var step = 0

var not_col = Color("#ffffe4")
var sel_col = Color("#9cb93b")

func _ready():
	visible = false

func _physics_process(delta):
	text.percent_visible = min(1, text.percent_visible + delta * SCROLL_SPEED)

	if Input.is_action_just_pressed("ui_accept"):
		if text.percent_visible < 1:
			text.percent_visible = 1
		else:
			step += 1
			if step >= data.size():
				visible = false
				emit_signal("CONVO_COMPLETE")
			else:
				_play_next()

	if answers.size() > 0 and text.percent_visible == 1:
		$choices.visible = true
		var val = 0
		for i in $choices.get_children():
			i.add_color_override("font_color", sel_col if val == selected else not_col)
			i.text = answers[val]
			val += 1

		if Input.is_action_just_pressed("ui_up") and selected > 0:
			selected -= 1
		if Input.is_action_just_pressed("ui_down") and selected < 2:
			selected += 1

	else:
		$choices.visible = false

func start_convo(d):
	visible = true
	data = d
	step = 0
	_play_next()

func _play_next():
	if "answers" in data[step]:
		answers = data[step].answers
	else:
		answers = []
	selected = 0
	text.percent_visible = 0
	text.text = data[step].text