extends KinematicBody2D

const ACCELERATION = 32
const SPEED = 80

onready var body = $body
onready var detector = $detector
onready var head = $head

var facing = "right"
var is_active = true
var velocity = Vector2(0, 0)

var allow = false

func _physics_process(delta):

	var i = 0
	if is_active and allow:
		if Input.is_action_pressed("ui_left"):
			i = -1
		elif Input.is_action_pressed("ui_right"):
			i = 1

	if abs(velocity.x) < .1 or not is_active:
		velocity.x = 0

	velocity.x = lerp(velocity.x, i * SPEED, ACCELERATION * delta)
	velocity = move_and_slide(velocity)

	var mode = "idle" if velocity.x == 0 else "walk"

	if velocity.x < 0:
		facing = "left"
	elif velocity.x > 0:
		facing = "right"

	body.set_animation(facing)
	head.set_animation("%s_%s" % [facing, mode])

func activate():
	allow = true

func on_haunt():
	$sounds/haunt.play()

func on_unhaunt():
	$sounds/unhaunt.play()