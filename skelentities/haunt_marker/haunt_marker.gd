extends Node2D

var direction = 0
var timer = 0

var end = Vector2(0, 0)
var start = Vector2(0, 0)

onready var haunt_1 = $haunt_1
onready var haunt_2 = $haunt_2

func _physics_process(delta):

	timer += direction * delta * 7.5

	if timer < 0:
		direction = 0
		timer = 0
	elif timer > 1:
		direction = 0
		timer = 1

	if timer == 0 or timer == 1:
		visible = false

	else:
		visible = true

		haunt_1.visible = (timer < .5)
		haunt_2.visible = (timer >= .5)

		position = start + (end - start) * timer