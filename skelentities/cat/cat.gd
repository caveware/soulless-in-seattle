extends Container

signal START_CONVO

var action = "TALK"
var text_offset = Vector2(-64, -16)

func use():
	emit_signal("START_CONVO")

func get_active():
	return true

func move():
	rect_position.x = 32