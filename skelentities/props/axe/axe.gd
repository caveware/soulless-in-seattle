extends Sprite

signal GOT

var action = ""
var text_offset = Vector2(0, 0)

func get_active():
	return false

func use():
	pass

func _physics_process(delta):
	if $detector.get_overlapping_bodies().size() > 0:
		emit_signal("GOT")