extends Sprite

var rat_gone = false

onready var face = $face

func hide_rat():
	face.visible = false

func show_rat():
	if not rat_gone:
		face.visible = true