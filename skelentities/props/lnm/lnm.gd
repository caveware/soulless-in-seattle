extends Container

signal ADD_METAL

const DURATION = .2

onready var sprite = $sprite
onready var tween = $tween

var force_hide = false
var shown = false

var hidden_position = Vector2(16, 37)
var shown_position = Vector2(16, 12)

func hide_lnm():
	if not shown:
		return

	shown = false

	tween.stop_all()
	tween.interpolate_property(sprite, "position", sprite.position, hidden_position, DURATION, Tween.TRANS_LINEAR, Tween.EASE_IN)
	tween.start()

func show_lnm():
	if force_hide or shown:
		return

	shown = true

	tween.stop_all()
	tween.interpolate_property(sprite, "position", sprite.position, shown_position, DURATION, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()

func shrink_away():
	force_hide = true

	sprite.animation = "shrink"
	sprite.playing = true
	sprite.play()
	tween.interpolate_property(sprite, "position", sprite.position, hidden_position, DURATION * 5, Tween.TRANS_LINEAR, Tween.EASE_IN, 3)
	tween.interpolate_callback(self, 3 + DURATION * 5, "_emit_metal_signal")
	tween.start()

func _emit_metal_signal():
	emit_signal("ADD_METAL")