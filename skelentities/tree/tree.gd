extends Container

signal DONE_CHOPPING
signal ON_CHOP

const MAX_CHOPS = 4

onready var tree = $tree
onready var tween = $tween

var action = "CHOP"
var active = true
var chop_level = 0
var has_axe = false
var text_offset = Vector2(-27, 66)

func use():
	if get_active():
		active = false
		var target = Vector2(tree.position.x, tree.position.y + 12)
		tween.interpolate_property(tree, "position", tree.position, target, .5,Tween.TRANS_BOUNCE, Tween.EASE_IN_OUT)
		tween.interpolate_callback(self ,1, "_post_chop")
		tween.start()
		chop_level += 1
		emit_signal("ON_CHOP")

func get_active():
	return active and has_axe

func _post_chop():
	if chop_level == MAX_CHOPS:
		emit_signal("DONE_CHOPPING")
	else:
		active = true