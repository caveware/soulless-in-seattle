extends Node2D

signal CLOSED
signal OPENED

onready var bottom = $bottom
onready var top = $top

var hold = true
var opening = true
var phase = 1

func _ready():
	hold = false

func _physics_process(delta):
	if hold:
		return

	var last_phase = phase
	phase = clamp(phase + (-1 if opening else 1) * delta * 4, 0, 1)

	if phase == 0 and last_phase > 0:
		emit_signal("OPENED")

	if phase == 1 and last_phase < 1:
		$tween.interpolate_callback(self, .2, "close")
		$tween.start()

	var inv_phase = 1 - phase
	top.position.y = -72 * inv_phase
	bottom.position.y = 72 + inv_phase * 72

	visible = phase > 0

func close():
	emit_signal("CLOSED")