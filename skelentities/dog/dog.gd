extends Container

signal COMPLETE
signal START_CONVO

var action = "TALK"
var active = true
var is_launched = false
var text_offset = Vector2(-64, -16)

func use():
	emit_signal("START_CONVO")

func get_active():
	return active

func _physics_process(delta):
	if is_launched:
		rect_position.x += 240 * delta

		if rect_position.x > 496:
			queue_free()
			emit_signal("COMPLETE")

func launch():
	is_launched = true
	$body.collision_layer = 0
	$body.collision_mask = 0
	$sprite.animation = "launched"